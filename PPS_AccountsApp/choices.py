###                                              choices.py                                           ###
###    Migrated all Address-based and Member-address "choice" lists into separate file for clarity    ###

COUNTRIES = (
    ('CAN', 'Canada'),
    ('USA', 'United States of America'),
)


STATES = (
    ('AK', 'Alaska'),
    ('AL', 'Alabama'),
    ('AR', 'Arkansas'),
    ('AS', 'American Samoa'),
    ('AZ', 'Arizona'),
    ('CA', 'California'),
    ('CO', 'Colorado'),
    ('CT', 'Connecticut'),
    ('DC', 'District of Columbia'),
    ('DE', 'Delaware'),
    ('FL', 'Florida'),
    ('FM', 'Federated States of Micronesia'),
    ('GA', 'Georgia'),
    ('GU', 'Guam'),
    ('HI', 'Hawaii'),
    ('IA', 'Iowa'),
    ('ID', 'Idaho'),
    ('IL', 'Illinois'),
    ('IN', 'Indiana'),
    ('KS', 'Kansas'),
    ('KY', 'Kentucky'),
    ('LA', 'Louisiana'),
    ('MA', 'Massachusetts'),
    ('MD', 'Maryland'),
    ('ME', 'Maine'),
    ('MI', 'Michigan'),
    ('MN', 'Minnesota'),
    ('MO', 'Missouri'),
    ('MP', 'Northern Mariana Islands'),
    ('MS', 'Mississippi'),
    ('MT', 'Montana'),
    ('NA', 'National'),
    ('NC', 'North Carolina'),
    ('ND', 'North Dakota'),
    ('NE', 'Nebraska'),
    ('NH', 'New Hampshire'),
    ('NJ', 'New Jersey'),
    ('NM', 'New Mexico'),
    ('NV', 'Nevada'),
    ('NY', 'New York'),
    ('OH', 'Ohio'),
    ('OK', 'Oklahoma'),
    ('OR', 'Oregon'),
    ('PA', 'Pennsylvania'),
    ('PR', 'Puerto Rico'),
    ('RI', 'Rhode Island'),
    ('SC', 'South Carolina'),
    ('SD', 'South Dakota'),
    ('TN', 'Tennessee'),
    ('TX', 'Texas'),
    ('UT', 'Utah'),
    ('VA', 'Virginia'),
    ('VI', 'Virgin Islands'),
    ('VT', 'Vermont'),
    ('WA', 'Washington'),
    ('WI', 'Wisconsin'),
    ('WV', 'West Virginia'),
    ('WY', 'Wyoming')
  )


PROVINCES = (
    ('AB', 'Alberta'),
    ('BC', 'British Columbia'),
    ('LB', 'Labrador'),
    ('MB', 'Manitoba'),
    ('NB', 'New Brunswick'),
    ('NF', 'Newfoundland'),
    ('NS', 'Nova Scotia'),
    ('NU', 'Nunavut'),
    ('NW', 'North West Terr.'),
    ('ON', 'Ontario'),
    ('PE', 'Prince Edward Is.'),
    ('QC', 'Quebec'),
    ('SK', 'Saskatchewen'),
    ('YU', 'Yukon')
)


PRONOUNS = (
    ('F', '\"she\" and \"her\"'),
    ('M', '\"he\" and \"him\"'),
    ('NP', '\"they\" and \"their\"'),
    ('NS1', '\"zie\" and \"zir\"'),
    ('NS2', '\"per\" and \"per\"'),
)


MEMBER_CLASS = (
    ('PS', 'Polio Survivor'),
    ('CG', 'Caregiver'),
    ('FM', 'Family Member'),
    ('HCP', 'Health Care Provider'),
    ('IO', 'Interested Other'),
    ('NR', 'Choose Not to Respond'),
)


MAIL_LIST_PARAMS = (
    ('AN', 'Announcements Only'),
    ('DD', 'Daily Digest'),
    ('IN', 'Individual E-mails'),
    ('WD', 'Weekly Digest'),
)


GROUP_TYPE = (
    ('IP', 'In Person'),
    ('PH', 'Phone Conference Call'),
    ('OL', 'Online'),
    ('MX', 'Mixed (In Person +)'),
)


OL_GROUP_TYPE = (
    ('AO', 'Audio Only'),
    ('CH', 'Chat'),
    ('FB', 'Facebook'),
    ('FT', 'FaceTime'),
    ('G+', 'Google+'),
    ('SK', 'Skype'),
    ('YG', 'Yahoo Groups'),
)