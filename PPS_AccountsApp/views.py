from django.shortcuts import render, redirect
from django.db.models import Q
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm

from .forms import MemberForm, AddressForm, SubAddressForm, SupportGroupForm, SubscripForm
from .models import Member
from PPS_DjangoApp.models import TextPage, ResourcePage
from PPS_AccountsApp.models import SupportGroup, InPersonSpptGrp


# Unauthenticated Views
def login_dialog(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('/')  # TODO: Find mechanism to redirect previous or next page.
    else:
        return redirect('invalid_error')


def logout_view(request):
    logout(request)
    return redirect('/')


def create_member(request):
    '''
    This view allows visitors to register for Membership so that they can see the Authenticated pages of the site.
    '''
    addrform = AddressForm(request.POST or None)
    regform = MemberForm(request.POST or None)
    user_form = UserCreationForm(request.POST or None)
    context = {'registration': regform, 'address': addrform, 'user_form': user_form}

    if addrform.is_valid() and regform.is_valid() and user_form.is_valid():
        user = user_form.save(commit=False)
        addr = addrform.save(commit=False)

        addr.save()
        reg = regform.save(commit=False)
        reg.set_password(request.POST['password'])
        reg.address = addr
        reg.user = user
        reg.save()
        return redirect('/')

    return render(request, 'create_member.html', context)


def create_sppt_grp(request):
    '''
    This view allows Support Group Coordinators to register the Group's information so that the listing can be found on
    the Survivor's Resource Listing page of the site.
    '''
    addrform = AddressForm(request.POST or None)
    grp_reg = SupportGroupForm(request.POST or None)
    context = {'grp_reg': grp_reg, 'address': addrform}
    import pdb;
    # pdb.set_trace()
    if addrform.is_valid() and grp_reg.is_valid():
        addr = addrform.save(commit=False)
        # pdb.set_trace()

        addr.save()
        grp_reg = grp_reg.save(commit=False)
        grp_reg.address = addr
        grp_reg.save()
        return redirect('/sppt_grp/'+grp_reg.slug)

    return render(request, 'create_sppt_grp.html', context)


def find_sppt_grp(request, slug='survivor-resources', state='OR'):
    '''
    This view allows a Member to locate either an online/phone Support Group, or a face-to-face Support Group within a
    preset distance radius of a specific geographical region (based on their current location when provided, otherwise
    based on a specific city name).
    '''

    page = ResourcePage.objects.get(slug=slug)
    ip_sppt_grps = InPersonSpptGrp.objects.filter(loc_addr__state=state)   #TODO: add geolocation support
    all_other_sppt_grps = SupportGroup.objects.filter(~Q(group_type='IP'))

    context = {'page': page, 'ip_sppt_grps': ip_sppt_grps, 'all_other_sppt_grps': all_other_sppt_grps}

    return render(request, 'find_sppt_grp.html', context)





# Authenticated Views
def account(request):
    '''
    This view allows Members to make changes to Member Account info, and therefore must accept both POST and GET
    requests.
    '''
    pagename = 'Manage Your Member Account'
    context = {'page_title': pagename}

    if AuthenticationForm.is_valid():
        auth = AuthenticationForm.save(commit=False)
        auth.save()
        return redirect('/')  # TODO: create AJAX for Json repopulation of page body

    return render(request, 'account.html', context)


def maillist(request):
    '''
    This view allows Members to join the Mailing List and to make or change selections regarding what mail they wish to
    receive and how often those messages should be sent. It therefore must accept both POST and GET requests.
    '''
    page = TextPage.objects.get(slug='join-our-mailing-list')

    subaddress_form = SubAddressForm(request.POST or None, instance=request.user.member.address)
    subform = SubscripForm(request.POST or None)


    if all(subform.is_valid(), subaddress_form.is_valid()):

        sub_info = subform.save(commit=False)
        sub_address = subaddress_form.save(commit=False)

        sub_info.subscriber = request.user

        sub_info.save()
        sub_address.save()


        return redirect('/')

    context = {'page': page,
               'sub_info': sub_info,
               'sub_address': sub_address}

    return render(request, 'maillist_reg.html', context)
