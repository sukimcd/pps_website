from django import forms
from PPS_AccountsApp.models import *

class AddressForm(forms.ModelForm):
    class Meta:
        model = Address
        fields = ['country', 'address1', 'address2', 'city', 'state', 'zipcode', 'province', 'postal_code',
                  'phone_number', 'fax_number', 'email_address', 'website']
        widgets = {
            'country': forms.Select(attrs={'class': 'btn btn-secondary btn-lg dropdown-toggle', 'id': 'country'}),
            'address1': forms.TextInput(attrs={'placeholder': 'First line of address:'}),
            'address2': forms.TextInput(attrs={'placeholder': 'Second line of address:'}),
            'city': forms.TextInput(attrs={'placeholder': 'Address City:'}),
            'state': forms.Select(attrs={'class': 'btn btn-secondary btn-lg dropdown-toggle', 'id': 'id_state'}),
            'zipcode': forms.TextInput(attrs={'placeholder': 'U.S. Address ZIP Code:'}),
            'province': forms.Select(attrs={'class': 'btn btn-secondary btn-lg dropdown-toggle', 'id': 'id_province'}),
            'postal_code': forms.TextInput(attrs={'placeholder': 'Canadian Address Postal Code:'}),
            'phone_number': forms.TextInput(attrs={'placeholder': 'Phone # (if any) you want other members to see:'}),
            'fax_number': forms.TextInput(attrs={'placeholder': 'Fax # (if any) you want other members to see:'}),
            'email_address': forms.TextInput(attrs={'placeholder': 'Your e-mail address:'}),
            'website': forms.TextInput(attrs={'placeholder': 'URL for website, if desired'}),
        }


class SubAddressForm(forms.Form):
    class Meta:
        model = Address
        fields = ['email_address', 'phone_number']
        widgets = {
            'email_address': forms.TextInput(attrs={'placeholder': 'E-mail address:'}),
            'phone_number': forms.TextInput(attrs={'placeholder': 'Phone number (if any) you want other members to see:'}),
        }


class SupportGroupForm(forms.ModelForm):
    class Meta:
        model = SupportGroup
        fields = ['name', 'group_type','is_restricted', 'building', 'room', 'contact_name', 'contact_email',
                  'contact_phone_number', 'mtg_start_time', 'mtg_end_time', 'mtg_repeat_pattern']
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Group name?'}),
            'group_type': forms.Select(attrs={'class': 'btn btn-secondary btn-lg dropdown-toggle'}),
            'building': forms.TextInput(attrs={'placeholder': 'Name of building?'}),
            'room': forms.TextInput(attrs={'placeholder': 'In what room?'}),
            'contact_name': forms.TextInput(attrs={'placeholder': 'Who do we contact for info about this group?'}),
            'contact_email': forms.TextInput(attrs={'placeholder': 'E-mail address for contact:'}),
            'contact_phone_number': forms.TextInput(attrs={'placeholder': 'Phone # for that person:?'}),
            'mtg_start_time': forms.TimeInput(attrs={'type': 'time'}),
            'mtg_end_time': forms.TimeInput(attrs={'type': 'time'}),
            'mtg_repeat_pattern': forms.TextInput(attrs={'placeholder': 'On what day/date? How often?'}),
        }


class InPersonSpptGrpForm(forms.ModelForm):
    class Meta:
        model = InPersonSpptGrp
        fields = ['loc_addr', 'facilitator_username']


class PhoneSpptGrpForm(forms.ModelForm):
    class Meta:
        model = PhoneSpptGrp
        fields = ['call_in_num', 'participant_key', 'facilitator_username']


class OnlineSpptGrpForm(forms.ModelForm):
    class Meta:
        model = OnlineSpptGrp
        fields = ['access_point', 'ol_group_type', 'facilitator_username']


class MemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = ['full_name', 'birthdate', 'gender', 'pronouns', 'bio', 'profile_img',
                  'member_type', 'subscribed', 'sppt_grp_memb', 'group_coord']
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Full name?'}),
            'gender': forms.TextInput(attrs={'placeholder':
                                             'Info about your gender or gender expression:'}),
            'pronouns': forms.Select(attrs={'class': 'btn btn-secondary btn-lg dropdown-toggle'}),
            'member_type': forms.Select(attrs={'class': 'btn btn-secondary btn-lg dropdown-toggle'}),
            'subscribed': forms.CheckboxInput(attrs={'class': 'checkbox'}),
            'sppt_grp_memb': forms.CheckboxInput(attrs={'class': 'checkbox'}),
            'group_coord': forms.CheckboxInput(attrs={'class': 'checkbox'}),
        }


class SubscripForm(forms.ModelForm):
    class Meta:
        model = Subscription
        fields = ['subscriber', 'active', 'recd_msg_freq']


class MailListRegForm(forms.ModelForm):
    username = forms.TextInput()
    class Meta:
        model = Member
        fields = ['full_name', 'subscribed' ]


class ListMsgForm(forms.ModelForm):
    class Meta:
        model = ListMessage
        fields = ['sending_user', 'subject', 'content']


class ChatRoom(forms.ModelForm):
    class Meta:
        model = ChatRoom
        fields = ['is_global', 'room_name', 'subject']


class ChatSession(forms.ModelForm):
    class Meta:
        model = ChatSession
        fields = ['topic', 'members', 'room_name']


class ChatMessage(forms.ModelForm):
    class Meta:
        model = ChatMessage
        fields = ['sending_user', 'content', 'session']
