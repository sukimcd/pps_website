from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify
from datetime import datetime
from PPS_DjangoFiles.settings import DAYS_SINCE_LAST_LOGIN, LOGIN_LAPSE_SINCE_JOINED, DAYS_SINCE_LAST_POST
from .choices import *


class Address(models.Model):
    '''
    In what country is this address located? \(Starting with USA and Canada only; choice determines which of state vs. province and ZIP vs. postal code will display\)
    '''
    country = models.CharField(max_length=50, null=True, blank=True, choices=COUNTRIES, default='USA')
    # What is the street address for the member or group?
    address1 = models.CharField(max_length=256, null=True, blank=True)
    # What is the second line \(Apt./Suite/Building/Room #\) of the street address?
    address2 = models.CharField(max_length=256, null=True, blank=True)
    # In what city is this address located?
    city = models.CharField(max_length=50, null=True, blank=True)
    # If this is a US address, in what state is it located?
    state = models.CharField(max_length=2, null=True, blank=True, choices=STATES, default='OR')
    # If this is a US address, what is its ZIP code?
    zipcode = models.PositiveSmallIntegerField(null=True, blank=True)
    # If this is a Canadian address, in what province is it located?
    province = models.CharField(max_length=2, null=True, blank=True, choices=PROVINCES, default='BC')
    # If this is a Canadian address, what is its postal code?
    postal_code = models.CharField(max_length=7, null=True, blank=True)
    # What phone number \(if any\) should Portland Area Post-Polio Support use to contact the member or group?
    phone_number = models.CharField(max_length=15, null=True, blank=True)
    # Is there a fax number that Portland Area Post-Polio Support should use to communicate with the member or group?
    fax_number = models.CharField(max_length=15, null=True, blank=True)
    # What e-mail address should Portland Area Post-Polio Support use to contact member or group?
    email_address = models.EmailField(null=False, blank=False)
    # Does the member or group have a website address that should be displayed for other members to see?
    website = models.URLField(null=True, blank=True)

    def __str__(self):
        if self.country == 'USA':
            return "{}, {}".format(self.city, self.state)
        else:
            return "{}, {}".format(self.city, self.province)


class SupportGroup(models.Model):
    # What do the group members call the group?
    name = models.CharField(max_length=50, null=True, blank=True)
    # What is the format of the group?
    group_type = models.CharField(max_length=2, choices=GROUP_TYPE)
    # Is the group restricted to only Polio Survivors?
    is_restricted = models.BooleanField(default=False)
    # Name of the person who answers inquiries about the group?
    contact_name = models.CharField(max_length=50, null=False, blank=False)
    # E-mail address of the person who answers inquiries about the group?
    contact_email = models.CharField(max_length=30, null=True, blank=True)
    # Phone number of the person who answers inquiries about the group?
    contact_phone_number = models.CharField(max_length=15, null=True, blank=True)
    '''
    If the meeting is located at a hospital, university, or other multi-building facility, in what building does
    the meeting take place?
    '''
    building = models.CharField(max_length=150, null=True, blank=True)
    # In what room is the meeting located?
    room = models.CharField(max_length=25, null=True, blank=True)
    # At what time does the meeting start?
    mtg_start_time = models.TimeField(auto_now_add=False)
    # At what time does the meeting end?
    mtg_end_time = models.TimeField(auto_now_add=False)
    # Meeting duration is automatically calculated based on the above two values.
    # mtg_duration = models.DurationField(null=True, blank=True)

    '''
    "Repeat Pattern" means that the meeting occurs
    a\) every week on <weekday>.
    b\) on the 1st/2nd/3rd/4th/5th/'last' <weekday> of every/every even-numbered/every odd-numbered/every <whatever> month.
    c\) on the 1st *and* 3rd/2nd *and* 4th <weekday> of every month.
    '''
    mtg_repeat_pattern = models.CharField(max_length=100, null=True, blank=True)
    # Next meeting date is automatically calculated based on the above algorithm.
    next_mtg_date = models.DateField(null=True, blank=True)
        
    def __str__(self):
        if self.name:
            return "{}, {}".format(self.name, self.contact_phone_number)
        else:
            return "{}, {}".format(self.contact_name, self.contact_phone_number)

    def save(self, *args, **kwargs):
        if self.name:
            self.slug = slugify(self.name)
        else:
            self.slug = slugify(self.contact_name)
        
        super(SupportGroup, self).save(*args, **kwargs)


class InPersonSpptGrp(SupportGroup):
    loc_addr = models.ForeignKey(Address, related_name='group_meets_at')
    facilitator_username = models.CharField(max_length=25, null=False, blank=False)

    def __str__(self):
        if self.name:
            return "{}, {}".format(self.name, self.loc_addr)
        else:
            return "{}, {}".format(self.contact_name, self.loc_addr)


class PhoneSpptGrp(SupportGroup):
    call_in_num = models.CharField(max_length=15, null=False, blank=False)
    participant_key = models.CharField(max_length=25, null=False, blank=False)
    facilitator_username = models.CharField(max_length=25, null=False, blank=False)

    def __str__(self):
        if self.name:
            return "{}, {}".format(self.name, self.call_in_num)
        else:
            return "{}, {}".format(self.contact_name, self.call_in_num)


class OnlineSpptGrp(SupportGroup):
    access_point = models.URLField(null=True, blank=True)
    ol_group_type = models.CharField(max_length=2, choices=OL_GROUP_TYPE)
    facilitator_username = models.CharField(max_length=25, null=False, blank=False)

    def __str__(self):
        if self.name is not None:
            return "{}, {}".format(self.name, self.ol_group_type)
        else:
            return "{}, {}".format(self.contact_name, self.ol_group_type)


class Member(models.Model):
    user = models.OneToOneField(User)
    full_name = models.TextField()
    address = models.ForeignKey(Address, related_name='memb_addr')
    birthdate = models.DateField(auto_now_add=False)
    joindate = models.DateField(auto_now_add=True)
    gender = models.TextField(null=True, blank=True)
    pronouns = models.CharField(max_length=3, null=True, blank=True, choices=PRONOUNS)
    bio = models.TextField()
    profile_img = models.ImageField(null=True, blank=True)
    profile_img_thumb = models.ImageField(null=True, blank=True)
    member_type = models.CharField(max_length=25, null=False, blank=False, choices=MEMBER_CLASS)
    subscribed = models.BooleanField(default=False, verbose_name='Subscribed to Mailing List')
    sppt_grp_memb = models.ManyToManyField(SupportGroup, verbose_name='Support Group Member', blank=True)
        # Reminder: ManyToManyField does not support "Default='False'" or "null='True'"
    group_coord = models.BooleanField(default=False, verbose_name='Group Coordinator')
    siteadmin = models.BooleanField(default=False)

    def __str__(self):
        return "{}, {}".format(self.full_name, self.address.state)

    def make_active(self):
        self.subscribed = True
        return self

    def check_active(self):
        checkdate = datetime.now()
        days_since_last_post = checkdate - self.listmessage.datetime_sent
        days_since_last_login = checkdate - self.last_login
        login_lapse_since_joined = self.last_login - self.joindate

        '''
        a member who has submitted to the mailing list within the last 90 days is automatically considered active.
        '''
        if self.listmessage is not None and days_since_last_post.days >= DAYS_SINCE_LAST_POST:
            return True  # ...and also update 'subscribed' field? Or 'active' field?

        elif days_since_last_login.days >= DAYS_SINCE_LAST_LOGIN or login_lapse_since_joined.days >= LOGIN_LAPSE_SINCE_JOINED:
            return False  # (because the member is *not* active if the above criteria are met) and send member an e-mail asking if they want to stay on the mailing list
        else:
            return True  # ...and also update 'subscribed' field? Or 'active' field?

    def make_inactive(self):
        if self.check_active() is False:
            # and no email response from member within 90 days:
            self.subscription.active = False
            return self



class Subscription(models.Model):
    subscriber = models.OneToOneField(Member)
    sub_date = models.DateTimeField(auto_created=True)
    active = models.BooleanField(default=True)
    recd_msg_freq = models.CharField(max_length=2, choices=MAIL_LIST_PARAMS, default='DD')

    def __str__(self):
        return "subscriber: {}, active: {}".format(self.subsciber, self.active)

    
class ListMessage(models.Model):
    sending_user = models.OneToOneField(Member)
    datetime_sent = models.DateTimeField(auto_now_add=True)
    subject = models.CharField(max_length=150, null=False, blank=False)
    content = models.TextField()

    def __str__(self):
        return "{}, {}".format(self.sending_user, self.datetime_sent)


class ChatRoom(models.Model):
    is_global = models.BooleanField(default=False)
    room_name = models.CharField(max_length=50, null=True, blank=True)
    subject = models.CharField(max_length=150, null=False, blank=False)

    def __str__(self):
        if self.room_name is not None:
            return "room_name: {}, subject: {}".format(self.room_name, self.subject)
        else:
            return self.subject


class ChatSession(models.Model):
    datetime_created = models.DateTimeField(auto_created=True)
    topic = models.CharField(max_length=50, null=False, blank=False)
    members = models.ManyToManyField(Member)
    datetime_ended = models.DateTimeField(null=True, blank=True)
    room_name = models.ForeignKey(ChatRoom)


    def __str__(self):
        return "{}, {}".format(self.topic, self.members)


class ChatMessage(models.Model):
    sending_user = models.OneToOneField(Member)
    datetime_sent = models.DateTimeField(auto_now_add=True)
    content = models.TextField()
    session = models.ForeignKey(ChatSession)

    def __str__(self):
        return "{}, {}".format(self.sending_user, self.datetime_sent)