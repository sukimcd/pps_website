###          PPS_DjangoFiles URL Configuration          ###

from django.conf.urls import include, url
from django.contrib import admin
from PPS_DjangoApp import views
from PPS_AccountsApp import views as acctsviews

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.home, name='pdx_pps_home'),
    url(r'^unauth_error/$', views.unauth_error),
    url(r'^resources/?$', views.resource_finder),
    url(r'^member/create/$', acctsviews.create_member, name='register'),
    url(r'^member/detail/(?P<pk>\d*)/$', acctsviews.account),
    #url(r'^member/update/(?P<pk>\d*)/$', acctsviews.update_member),
    url(r'^sppt_grp/create/$', acctsviews.create_sppt_grp, name='grp_reg'),
    url(r'^sppt_grp/find/$', acctsviews.find_sppt_grp, name='find_sppt_grp'),
    url(r'^resources/(?P<slug>[a-zA-Z0-9\-]*)', views.resource_type),
    url(r'^contact/', views.contact),  # TODO:  make dynamic
    url(r'^maillist_reg/', acctsviews.maillist, name='maillist_reg'),
    url(r'^login_dialog/', acctsviews.login_dialog, name='login_dialog')
 ]
