from selenium import webbdriver
from selenium.webdriver.common.keys import keys

def setup:
    driver = webdriver.Safari()
    driver.maximize_window()
    driver

class TestPage:
    def test_default_page_style(self):
        driver.get("http://localhost:8000/pdx_pps_home.html")
        assert 'PPS' in driver.title


class TestTextPage(TestPage):
    def test_textpg_text_flow(self):
        driver.get("http://localhost:8000/Text_Page.html")
        pass


class TestResourcePage(TestPage):
    def test_rsrcpg_nav_correct_4rsrc_type(self):
        pass


class TestResourceListing(TestPage):
    def country_dependent_params(self):
        driver.get("http://localhost:8000/Resource_Page.html")
        #test_usa_listg_loc_has_state(self):
        #test_canada_listg_loc_has_prov(self):
        pass