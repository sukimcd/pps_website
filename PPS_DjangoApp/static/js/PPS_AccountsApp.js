//          JavaScript/JQuery file for AccountsApp Module         //

$('#id_province').hide();
$('#id_postal_code').hide();

$('#country').change(function (evt){
    if (evt.target.value === 'USA') {
        $('#id_province').hide();
        $('#id_postal_code').hide();
        $('#id_state').show();
        $('#id_zipcode').show();
    } else {
        $('#id_province').show();
        $('#id_postal_code').show();
        $('#id_state').hide();
        $('#id_zipcode').hide();   
    }  
});

$( function() {
    $( "#id_birthdate" ).datepicker();
  } );