from django.test import TestCase
import unittest
from unittest import skip
from .models import Page, TextPage, ResourcePage, ResourceListing
from .fixtures import *

# Creating unit tests for DjangoApp '.models' module.

class TestPage(TestCase):
    def setup(self):
        self.goodpage = Page.objects.create(title=default_good_page_title,
                                            slug=default_good_page_slug)

        self.goodtextpg = TextPage.objects.create(title=default_good_textpage_title,
                                                  slug=default_good_textpage_slug,
                                                  text_content= test_text_quote, test_text_translation,
                                                    test_text_body)

        self.goodrsrcpg = ResourcePage.objects.create(title=default_good_resourcepage_title,
                                                      slug=default_good_resourcepage_slug)

        self.goodrsrclistg = ResourceListing.objects.create(title=default_good_listingtitle,
                                                            slug=default_good_listing_slug,
                                                            page=self.goodresourcepage,
                                                            website=good_test_url,
                                                            source=good_test_source,
                                                            location=good_us_test_address)

    def test_goodpage_params_exist(self):
        self.assertIsNotNone(self.goodpage.title)
        self.assertIsNotNone(self.goodpage.slug)


    def setup(self):
        self.badpage = Page.objects.create(title=default_bad_page_title,
                                            slug=default_bad_page_slug)

        self.badtextpg = TextPage.objects.create(title=default_badtextpg_title,
                                                  slug=default_badtextpg_slug,
                                                  text_content=badtext_quote, badtext_translation,
                                                  badtext_body)

        self.badrsrcpg = ResourcePage.objects.create(title=default_badrsrcpage_title,
                                                      slug=default_badrsrcpage_slug)

        self.badrsrclistg = ResourceListing.objects.create(title=default_badlistg_title,
                                                            slug=default_badlistg_slug,
                                                            page=self.badrsrcpage,
                                                            website=bad_test_url,
                                                            source=bad_test_source,
                                                            location=bad_us_test_address)


    def test_badpage_params_fail(self):
        self.assertIsNotNone(self.badpage.title)
        self.assertIsNotNone(self.badpage.slug)


class TestTextPage(TestPage):
    def test_goodtextpg_params_exist(self):
        self.assertIsNotNone(self.goodtextpage.title)
        self.assertIsNotNone(self.goodtextpage.slug)


class TestResourcePage(TestPage):
    def test_rsrcpg_params_exist(self):
        self.assertIsNotNone(self.goodrsrcpage.title)
        self.assertIsNotNone(self.goodrsrcpage.slug)


class TestResourceListing(TestPage):
    def test_listg_params_exist(self):
        self.assertIsNotNone(self.goodlistg.title)
        self.assertIsNotNone(self.goodlistg.slug)

    # need to figure out how to validate when to skip
    # @unittest.skipUnless(goodrsrclistg.website == '', "URL field is not blank")
    # def test_listg_url_canb_blank(self):

    @unittest.skip("Skip until source params defined")
    def test_listg_has_source(self):
        pass

    @unittest.skipUnless("Source field is not populated")
    def test_listg_source_cantb_blank(self):
        pass

