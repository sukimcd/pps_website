default_good_page_title = 'Post-Polio Syndrome Information and Resources'
default_good_page_slug = 'home'


default_good_textpg_title = 'PPS_good_textpg/lorem'
default_good_textpg_slug = 'lorem'
good_text_title = 'Lorem Ipsum\n'

good_text_quote = '\"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci \
                            velit...\"\n '

good_text_translation =  '\"There is no one who loves pain itself, who seeks after it and wants to have it, \
                                 simply because it is pain...\"\n \n '

good_text_body = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ultrices, felis quis tempor malesuada, \
                    ante dolor auctor velit, eget tincidunt mauris nisl sit amet arcu. Proin auctor massa quis dolor \
                    gravida tincidunt. Sed cursus at tellus et porttitor. Pellentesque urna turpis, volutpat sit amet \
                    sapien id, pellentesque lacinia ligula. Aenean cursus orci a ultrices hendrerit. Fusce fringilla \
                    odio id ipsum placerat, vitae rhoncus dui tincidunt. Praesent  vestibulum, ligula at molestie \
                    rutrum, mauris libero vulputate odio, eleifend scelerisque erat urna at lorem. Duis maximus \
                    porttitor eros ut blandit. Curabitur vel ultrices eros. Cras vehicula nunc risus, ac hendrerit lacus \
                    consequat eu. Nulla fringilla tempor leo et sagittis. Morbi volutpat orci quis consectetur tempus. \
                    Nunc consequat ex augue, pharetra porta quam ultrices eu. Quisque finibus felis non venenatis \
                    malesuada. Aliquam et nisl libero. Etiam tincidunt dapibus magna et maximus.'



default_good_rsrcpg_title = 'Post_Polio Symdrome Resources for Survivors'
default_good_rsrcpg_slug = 'survivors'


default_good_listg_title = 'PPS_survivor_resources.html/suptgrp/oregon/Hillsboro'
default_good_listg_slug = 'hillsboro'

good_us_test_address = {'building': 'Tuality Community Hospital', 'room': 'blank', 'street_address': '335 SE 8th Ave.',
                        'city': 'Hillsboro', 'state': 'OR', 'zip': '97123', 'country': 'USA'}
good_test_url = 'http://international-post-polio-support.org/'
good_test_source = 'http://www.post-polio.org/net/PDIR.pdf'


#todo: create bad data models

