from django.db import models
from django.utils.text import slugify
from PPS_AccountsApp.models import Address


class Page(models.Model):
    title = models.CharField(max_length=50, null=False, blank=False)
    slug = models.SlugField(null=True, blank=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Page, self).save(*args, **kwargs)


class TextPage(Page):
    content = models.TextField(null=False, blank=False)

    def __str__(self):
        return self.title


class ResourcePage(Page):
    type = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        message = "type:{} title:{}".format(str(self.type), str(self.title))
        return message


class ResourceListing(models.Model):
    page = models.ForeignKey(ResourcePage, related_name='listing')
    website = models.URLField(null=True, blank=True)
    source = models.TextField(null=False)
    title = models.CharField(max_length=250)
    description = models.TextField(null=False, blank=False)
    location = models.OneToOneField(Address)

    def __str__(self):
        return self.title, self.location.city